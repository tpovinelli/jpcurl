module JPCurl {
  requires jpedit;
  requires javafx.base;
  requires javafx.controls;
  exports jpplugin.jpcurl;
  opens jpplugin.jpcurl to javafx.base, jpedit;
}