package jpplugin.jpcurl;

import com.tom.jpedit.gui.JPEditWindow;
import com.tom.jpedit.plugins.JPEditPlugin;
import com.tom.jpedit.plugins.PluginProperties;
import com.tom.jpedit.plugins.components.PluginMenuItem;
import com.tom.jpedit.plugins.components.PluginOwnedComponent;
import com.tom.jpedit.plugins.components.PluginToolbarButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextInputDialog;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Optional;

public class JPCurl implements JPEditPlugin {
  @Override
  public void onPluginLoad(List<JPEditWindow> list) throws Exception {

  }

  @Override
  public void onNewWindow(List<JPEditWindow> list, JPEditWindow jpEditWindow) {

  }

  @Override
  public void onExit() {

  }

  @Override
  public PluginProperties pluginProperties() {
    PluginToolbarButton button = new PluginToolbarButton("Curl");
    button.setOnAction(getButtonHandler(button));
    PluginMenuItem item = new PluginMenuItem("Curl");
    item.setOnAction(getButtonHandler(item));
    return new PluginProperties(item, null, button);
  }

  private <T extends PluginOwnedComponent> EventHandler<ActionEvent> getButtonHandler(T component) {
    return event -> {
      String xurl = promptForInput("Enter URL", "Enter URL", "Enter the URL to fetch").orElse(null);
      if (xurl == null) return;
      try {
        URL url = new URL(xurl);
        try (InputStream stream = url.openStream()) {
          byte[] bytes = stream.readAllBytes();
          String content = new String(bytes);
          component.getOwner().getTextArea().insertText(0, content);
        }
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    };
  }

  private static Optional<String> promptForInput(String title, String header, String msg) {
    var dialog = new TextInputDialog(title);
    dialog.setTitle(title);
    dialog.setHeaderText(header);
    dialog.setContentText(msg);
    return dialog.showAndWait();
  }
}
